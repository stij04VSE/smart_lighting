import 'package:flutter/material.dart';
import 'package:lighting_control/models/control.dart';
import 'package:lighting_control/models/control_state.dart';
import 'package:lighting_control/services/set_control_states.dart';
import 'package:lighting_control/theme/colors.dart';
import 'package:lighting_control/theme/text_styles.dart';

class ControlButton extends StatefulWidget {
  final Control control;
  final ControlState? controlState;

  const ControlButton({Key? key, required this.control, this.controlState})
      : super(key: key);

  @override
  State<ControlButton> createState() => _ControlButtonState();
}

class _ControlButtonState extends State<ControlButton> {
  static const colors = AppColors();
  static TextStyles textStyles = TextStyles();
  final SetControlState _setControlState = SetControlState();

  @override
  Widget build(BuildContext context) {
    return TextButton.icon(
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
                widget.controlState?.value != null
                    ? widget.controlState?.value == widget.control.feedbackValue
                        ? colors.controlButtonOn
                        : colors.controlButtonOff
                    : colors.controlButtonOff),
            foregroundColor: MaterialStateProperty.all<Color>(colors.controlButtonIcon),
            minimumSize: MaterialStateProperty.all<Size>(const Size.fromHeight(double.infinity)), // vyplní rodiče
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)))),
        onPressed: () {
          if (widget.control.commands != null) {
            _setControlState.setControlState(widget.control.commands!);
          }
        },
        icon: widget.control.commands?.first.command == 'increaseValue'
            ? const Icon(Icons.expand_less, size: 70.0)
            : widget.control.commands?.first.command == 'decreaseValue'
                ? const Icon(Icons.expand_more, size: 70.0)
                : Container(),
        label: Text(
          widget.controlState?.value != null ? widget.control.name : '',
          style: textStyles.controlButton,
        ));
  }

}
