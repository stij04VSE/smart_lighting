import 'package:flutter/material.dart';
import 'package:lighting_control/models/control.dart';
import 'package:lighting_control/models/control_state.dart';
import 'package:lighting_control/theme/colors.dart';
import 'package:lighting_control/theme/text_styles.dart';
import 'package:percent_indicator/percent_indicator.dart';

class ControlLight extends StatefulWidget {
  final Control control;
  final ControlState? controlState;

  const ControlLight({Key? key, required this.control, required this.controlState}) : super(key: key);

  @override
  State<ControlLight> createState() => _ControlLightState();
}

class _ControlLightState extends State<ControlLight> {
  static const colors = AppColors();
  static TextStyles textStyles = TextStyles();

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return LinearPercentIndicator(
          padding: const EdgeInsets.all(0),
          barRadius: const Radius.circular(10.0),
          //width: constraints.maxWidth,
          lineHeight: constraints.maxHeight,
          backgroundColor: colors.controlLightOff,
          progressColor: colors.controlLightOn,
          percent: widget.controlState?.value != null ? widget.controlState?.value / 100 : 0,
          center: Text(
              '${widget.controlState?.value}%',
              style: textStyles.controlLight
          ),
        );
      }
    );
  }

}
