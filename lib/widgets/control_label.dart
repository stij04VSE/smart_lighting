import 'package:flutter/material.dart';
import 'package:lighting_control/models/control.dart';
import 'package:lighting_control/theme/colors.dart';
import 'package:lighting_control/theme/text_styles.dart';

class ControlLabel extends StatelessWidget {
  static const colors = AppColors();
  static TextStyles textStyles = TextStyles();
  final Control control;

  const ControlLabel({Key? key, required this.control}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: colors.controlLabel,
        borderRadius: const BorderRadius.all(Radius.circular(10.0))
      ),
      child: Center(
          child: Text(
              control.name,
              style: textStyles.controlLabel
          )
      )
    );
  }

}
