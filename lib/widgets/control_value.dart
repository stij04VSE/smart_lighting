import 'package:flutter/material.dart';
import 'package:lighting_control/models/control.dart';
import 'package:lighting_control/models/control_state.dart';
import 'package:lighting_control/theme/colors.dart';
import 'package:lighting_control/theme/text_styles.dart';

class ControlValue extends StatelessWidget {
  static const colors = AppColors();
  static TextStyles textStyles = TextStyles();
  final Control control;
  final ControlState? controlState;

  const ControlValue({Key? key, required this.control, required this.controlState}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: colors.controlValue,
        borderRadius: const BorderRadius.all(Radius.circular(10.0)),
      ),
      child: Center(
        child: Text(
            controlState?.value.toString() ?? 'null',
            style: textStyles.controlValue
        )
      )
    );
  }

}
