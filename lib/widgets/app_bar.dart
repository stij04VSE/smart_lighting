import 'dart:async';
import 'package:flutter/material.dart';
import 'package:lighting_control/theme/colors.dart';
import 'package:lighting_control/theme/text_styles.dart';
import 'package:provider/provider.dart';
import 'package:lighting_control/providers/page_name_provider.dart';
import 'package:intl/intl.dart';

class CustomAppBar extends StatelessWidget {
  static const colors = AppColors();
  static TextStyles textStyles = TextStyles();
  final String? pageName;

  const CustomAppBar({Key? key, this.pageName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
        backgroundColor: CustomAppBar.colors.background,
        elevation: 0,
        title: Row(
          children: [
            SizedBox(
              height: 35.0,
              child: Container(
                color: colors.panel,
                child: Image.asset(
                  'assets/logo_and_company_name.png',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Expanded(
              child: Center(
                child: PageName(pageName: pageName),
              ),
            ),
            const SizedBox(
                width: 205.0,
                height: 35.0,
                child: Center(
                    child: CurrentTime()
                )
            )
          ],
        )
    );
  }
}

class PageName extends StatelessWidget {
  static TextStyles textStyles = TextStyles();
  final String? pageName;

  const PageName({Key? key, this.pageName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(pageName ?? context.watch<PageNameProvider>().pageName,
        style: textStyles.title);
  }
}

class CurrentTime extends StatefulWidget {
  const CurrentTime({Key? key}) : super(key: key);

  @override
  State<CurrentTime> createState() => _CurrentTimeState();
}

class _CurrentTimeState extends State<CurrentTime> {
  static TextStyles textStyles = TextStyles();
  late Timer _timer;
  late String _currentDateTime;
  late String _newDateTime;

  @override
  void initState() {
    super.initState();
    _newDateTime = _formatDateTime(DateTime.now());
    _currentDateTime = _newDateTime;
    _timer = Timer.periodic(
        const Duration(seconds: 1), (Timer t) => _refreshDateTime());
  }

  void _refreshDateTime() {
    _newDateTime = _formatDateTime(DateTime.now());
    if (_currentDateTime != _newDateTime) {
      _currentDateTime = _newDateTime;
      setState(() {});
    }
  }

  String _formatDateTime(DateTime dateTime) {
    return DateFormat('H:mm   d.M.yyyy').format(dateTime);
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Text(_currentDateTime, style: textStyles.dateTime);
  }
}
