import 'package:flutter/material.dart';
import 'package:lighting_control/screens/passcode_screen.dart';
import 'package:lighting_control/theme/colors.dart';
import 'package:lighting_control/widgets/app_bar.dart';
import 'package:lighting_control/services/get_configuration.dart';
import 'package:lighting_control/models/device.dart';
import 'package:lighting_control/widgets/page_view.dart';
import 'package:lighting_control/theme/text_styles.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  static const colors = AppColors();
  static TextStyles textStyles = TextStyles();
  final GetConfiguration _configuration = GetConfiguration();
  Device? device;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: colors.background,
        appBar: const PreferredSize(
          preferredSize: Size.fromHeight(56.0), // výška appbaru
          child: CustomAppBar(),
        ),
        drawer: SafeArea(
          child: Drawer(
            backgroundColor: colors.panel,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 100,
                  color: colors.background,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(20.0, 10.0, 0, 10.0),
                    child: Row(
                      children: [
                        Container(
                          color: colors.panel,
                          child: Image.asset('assets/logo_company.png', fit: BoxFit.cover)
                        ),
                        const SizedBox(width: 30.0),
                        Text(device?.deviceName ?? 'Touch control',
                            style: textStyles.deviceName)
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                SizedBox(
                  width: double.infinity,
                  child: TextButton.icon(
                    style:
                        TextButton.styleFrom(alignment: Alignment.centerLeft),
                    icon: Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 20.0, 3.0, 20.0),
                      child: Icon(Icons.refresh, color: colors.text),
                    ),
                    label: Padding(
                      padding: const EdgeInsets.fromLTRB(3.0, 20.0, 20.0, 20.0),
                      child: Text('Reload configuration',
                          style: textStyles.menuItem),
                    ),
                    onPressed: () async {
                      device = await _configuration.getDeviceConfiguration();
                      Navigator.pop(context);
                      setState(() {});
                    },
                  ),
                ),
                /*
                SizedBox(
                  width: double.infinity,
                  child: TextButton.icon(
                    style:
                        TextButton.styleFrom(alignment: Alignment.centerLeft),
                    icon: Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 20.0, 3.0, 20.0),
                      child: Icon(Icons.delete, color: colors.text),
                    ),
                    label: Padding(
                      padding: const EdgeInsets.fromLTRB(3.0, 20.0, 20.0, 20.0),
                      child: Text('Wipe data', style: textStyles.menuItem),
                    ),
                    onPressed: () {
                      device = null;
                      Navigator.pop(context);
                      context
                          .read<PageNameProvider>()
                          .setPageName('Lighting control panel');
                      setState(() {});
                    },
                  ),
                ),
                 */
                SizedBox(
                  width: double.infinity,
                  child: TextButton.icon(
                    style:
                        TextButton.styleFrom(alignment: Alignment.centerLeft),
                    icon: Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 20.0, 3.0, 20.0),
                      child: Icon(Icons.cleaning_services, color: colors.text),
                    ),
                    label: Padding(
                      padding: const EdgeInsets.fromLTRB(3.0, 20.0, 20.0, 20.0),
                      child: Text('Clean screen', style: textStyles.menuItem),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/cleaning');
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 28.0),
                  child: Divider(
                    thickness: 1.0,
                    color: colors.text,
                  ),
                ),
                SizedBox(
                  width: double.infinity,
                  child: TextButton.icon(
                    style:
                        TextButton.styleFrom(alignment: Alignment.centerLeft),
                    icon: Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 20.0, 3.0, 20.0),
                      child: Icon(Icons.settings, color: colors.text),
                    ),
                    label: Padding(
                      padding: const EdgeInsets.fromLTRB(3.0, 20.0, 20.0, 20.0),
                      child: Text('Settings', style: textStyles.menuItem),
                    ),
                    onPressed: _showLockScreen
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.bottomLeft,
                    child: Padding(
                      padding:
                          const EdgeInsets.fromLTRB(28.0, 10.0, 20.0, 30.0),
                      child: Text('Version 1.01', style: textStyles.version),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        body: device != null
            ? CustomPageView(device: device!)
            : Center(child: Text('NO DATA', style: textStyles.noData)));
  }

  void _showLockScreen() {
    Navigator.push(context, PageRouteBuilder(
        opaque: false,
        pageBuilder: (context, animation, secondaryAnimation) => const CustomPasscodeScreen())
    );
  }

}
