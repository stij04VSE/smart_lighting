import 'package:flutter/material.dart';
import 'package:lighting_control/services/settings_preferences.dart';
import 'package:lighting_control/widgets/app_bar.dart';
import 'package:lighting_control/theme/colors.dart';
import 'package:lighting_control/theme/text_styles.dart';
import 'package:flutter_settings_ui/flutter_settings_ui.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  static const colors = AppColors();
  static TextStyles textStyles = TextStyles();
  late TextEditingController _controller;
  late String _apiEndpoint;
  late bool _isHttpsEnabled;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
    _apiEndpoint = SettingsPreferences.getApiEndpoint() ?? '';
    _controller.text = _apiEndpoint;
    _isHttpsEnabled = SettingsPreferences.getIsHttpsEnabled() ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colors.background,
      appBar: const PreferredSize(
          preferredSize: Size.fromHeight(56.0), // výška appbaru
          child: CustomAppBar(
            pageName: 'Settings',
          )),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: SettingsList(backgroundColor: colors.background, sections: [
          SettingsSection(
            title: 'General',
            titleTextStyle: textStyles.settingsSectionTile,
            titlePadding: const EdgeInsets.only(left: 20.0, bottom: 5.0),
            tiles: [
              SettingsTile(
                title: 'API Endpoint',
                subtitle: _apiEndpoint,
                titleTextStyle: textStyles.settingsTileTitle,
                subtitleTextStyle: textStyles.settingsTileSubtitle,
                theme: SettingsTileTheme(
                    iconColor: colors.text,
                    textColor: colors.text,
                    tileColor: colors.panel,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0))),
                leading: const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(Icons.cloud_outlined),
                ),
                trailing: const Icon(Icons.keyboard_arrow_right, size: 40.0),
                onPressed: (context) async {
                  final apiEndpoint = await openDialog(context);
                  if (apiEndpoint == null || apiEndpoint.isEmpty) return;
                  await SettingsPreferences.setApiEndpoint(apiEndpoint);
                  _apiEndpoint = apiEndpoint;
                  setState(() {});
                },
              ),
            ],
          ),
          SettingsSection(
            title: 'Security',
            titleTextStyle: textStyles.settingsSectionTile,
            titlePadding:
                const EdgeInsets.only(left: 20.0, top: 20.0, bottom: 5.0),
            tiles: [
              SettingsTile.switchTile(
                title: 'Enable HTTPS protocol',
                titleTextStyle: textStyles.settingsTileTitle,
                switchActiveColor: colors.buttonOn,
                theme: SettingsTileTheme(
                    iconColor: colors.text,
                    textColor: colors.text,
                    tileColor: colors.settingsTile,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0))),
                leading: const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(Icons.https_outlined),
                ),
                switchValue: _isHttpsEnabled,
                onToggle: (bool isHttpsEnabled) async {
                  await SettingsPreferences.setIsHttpsEnabled(isHttpsEnabled);
                  _isHttpsEnabled = isHttpsEnabled;
                  setState(() {});
                },
              ),
            ],
          ),
        ]),
      ),
    );
  }

  Future<String?> openDialog(BuildContext context) => showDialog<String>(
        context: context,
        builder: (context) => AlertDialog(
          backgroundColor: colors.panel,
          title: const Text('Enter API Endpoint'),
          titleTextStyle: textStyles.settingsTileTitle,
          content: SizedBox(
            width: MediaQuery.of(context).size.width / 2.0,
            child: TextField(
              controller: _controller,
              autofocus: true,
              style: textStyles.settingsTextField,
              cursorColor: colors.buttonOn,
              decoration: InputDecoration(
                labelStyle: textStyles.settingsTileSubtitle,
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: colors.buttonOn),
                ),
                border: InputBorder.none,
              ),
            ),
          ),
          actions: [
            TextButton(
                child: Text('Submit', style: textStyles.settingsSubmit),
                onPressed: () {
                  Navigator.of(context).pop(_controller.text);
                }),
            Text(' | ', style: textStyles.settingsSectionTile),
            TextButton(
                child: Text('Cancel', style: textStyles.settingsSubmit),
                onPressed: () {
                  Navigator.of(context).pop();
                  _controller.text = SettingsPreferences.getApiEndpoint() ?? '';
                })
          ],
        ),
      );
}
