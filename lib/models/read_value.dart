class ReadValue {
  final String GwID;
  final String property;

  ReadValue({
    required this.GwID,
    required this.property
  });

  factory ReadValue.fromJson(Map<String, dynamic> json) {
    return ReadValue(
        GwID: json['GwID'] as String,
        property: json['property'] as String
    );
  }

}
