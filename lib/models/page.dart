import 'package:lighting_control/models/control.dart';

class Page {
  final String name;
  final int width;
  final int height;
  final List<Control> controls;

  const Page({
    required this.name,
    required this.width,
    required this.height,
    required this.controls
  });

  factory Page.fromJson(Map<String, dynamic> json) {
    var controlsObjJson = json['controls'] as List;
    List<Control> _controls = controlsObjJson.map((controlJson) => Control.fromJson(controlJson)).toList();
    return Page(
        name: json['name'] as String,
        width: json['width'] as int,
        height: json['height'] as int,
        controls: _controls
    );
  }

}