class Command {
  final String GwID;
  final String property;
  final String command;
  dynamic value;

  Command({
    required this.GwID,
    required this.property,
    required this.command,
    required this.value
  });

  factory Command.fromJson(Map<String, dynamic> json) {
    return Command(
        GwID: json['GwID'] as String,
        property: json['property'] as String,
        command: json['command'] as String,
        value: json['value']
    );
  }

}