import 'package:http/http.dart' as http;
import 'package:lighting_control/services/settings_preferences.dart';

class RegisterDevice {
  Future<void> registerDevice() async {
    await http.post(Uri.parse('${SettingsPreferences.getCommunicationProtocol()}://${SettingsPreferences.getApiEndpoint()}'),
        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body:
        'c={"value": "hello"}'
    );
  }

}