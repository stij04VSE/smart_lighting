import 'package:shared_preferences/shared_preferences.dart';

class SettingsPreferences {
  static late SharedPreferences _preferences;

  static const _keyApiEndpoint = 'key-apiEndpoint';
  static const _keyIsHttpsEnabled = 'key-isHttpsEnabled';

  static Future init() async => _preferences = await SharedPreferences.getInstance();

  static Future setApiEndpoint(String apiEndpoint) async => await _preferences.setString(_keyApiEndpoint, apiEndpoint);

  static String? getApiEndpoint() => _preferences.getString(_keyApiEndpoint);

  static Future setIsHttpsEnabled(bool isHttpsEnabled) async => await _preferences.setBool(_keyIsHttpsEnabled, isHttpsEnabled);

  static bool? getIsHttpsEnabled() => _preferences.getBool(_keyIsHttpsEnabled);

  static String getCommunicationProtocol() {
    if (getIsHttpsEnabled() != null) {
      return getIsHttpsEnabled()! ? 'https' : 'http';
    } else {
      return 'http';
    }
  }

}