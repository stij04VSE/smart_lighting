import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:lighting_control/models/device.dart';
import 'package:flutter/services.dart';
import 'package:json_schema2/json_schema2.dart';
import 'package:lighting_control/services/settings_preferences.dart';

class GetConfiguration {
  String? errorMessage;

  Future<Device> _getConfiguration() async {
    var url = Uri.parse('${SettingsPreferences.getCommunicationProtocol()}://${SettingsPreferences.getApiEndpoint()}get=20');
    final response = await http.get(url);

    if (response.statusCode == 200) {
      final configurationSchema = await rootBundle.loadString('assets/configuration_schema.json');
      final jsonSchema = JsonSchema.createSchema(configurationSchema);
      final decodedJson = jsonDecode(response.body);

      if (!jsonSchema.validate(decodedJson)) {
        throw Exception('JSON Schema validation failed! (configuration)');
      }

      return Device.fromJson(decodedJson);
    } else {
      throw Exception('Failed to load configuration!');
    }
  }

  Future<Device?> getDeviceConfiguration() async {
    try {
      errorMessage = null;
      GetConfiguration instance = GetConfiguration();
      return await instance._getConfiguration();
    } catch (e) {
      errorMessage = "Couldn't fetch device configuration";
    }
    return null;
  }


}
