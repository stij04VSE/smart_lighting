import 'package:lighting_control/models/control_state.dart';
import 'package:mobx/mobx.dart';
import 'package:lighting_control/services/get_control_state.dart';
import 'package:lighting_control/models/control.dart';

part 'control_store.g.dart';

class ControlStore extends _ControlStore with _$ControlStore {
  ControlStore(GetControlState getControlState) : super(getControlState);
}

enum StoreState { initial, loading, loaded }

abstract class _ControlStore with Store {
  final GetControlState _getControlState;

  _ControlStore(this._getControlState);

  @observable
  ObservableFuture<ControlState>? _controlStateFuture;

  @observable
  ControlState? controlState;

  @observable
  String? errorMessage;

  @computed
  StoreState get state {
    if (_controlStateFuture == null || _controlStateFuture?.status == FutureStatus.rejected) {
      return StoreState.initial;
    }
    return _controlStateFuture?.status == FutureStatus.pending
        ? StoreState.loading
        : StoreState.loaded;
  }

  @action
  Future getControlState(Control control) async {
    try {
      errorMessage = null;
      _controlStateFuture = ObservableFuture(_getControlState.getControlState(control));
      controlState = await _controlStateFuture;
      //controlState = await _getControlState.getControlState(control);
    } catch (e) {
      errorMessage = "Couldn't fetch control state";
    }
  }

}



